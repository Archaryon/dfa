#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <cstring>
using namespace std;
class AutomatFinitDeterminist;
struct Acceptor
{
	vector <char> CaractereAcceptate;
	friend class AutomatFinitDeterminist;
};
class AutomatFinitDeterminist{
int nr_stari = 0;
string word;
vector <int> stari_finale;
Acceptor Automat[100][100];
int StareInitiala;
public:
    void citire();
    int TestFinalizare(int);
    int parcurgere();
    void GetWord(){if(word=="$")cout<<"lambda";else cout<<word;}
    int VerificareLambda();
};
void AutomatFinitDeterminist::citire()
{
	ifstream f("Intrare.in");
	f >> nr_stari;
	f >> word;
	int i, x,nr_tranzitii,k,j,n;
		f >> x;
		StareInitiala=x-1;
		f >> n;
		for(i=0;i<n;i++)
		{f >> x;
            stari_finale.push_back(x-1);}
	char litera;
	f >> nr_tranzitii;
	while (nr_tranzitii != 0)
	{
		f >> i >> j >> litera;
		Automat[i-1][j-1].CaractereAcceptate.push_back(litera);
		nr_tranzitii--;
	}
	for (i = 0; i < nr_stari; i++)
	{
		for (j = 0; j < nr_stari; j++)
		{
			if (Automat[i][j].CaractereAcceptate.size() == 0)
				cout << '0';
			for (k = 0; k < Automat[i][j].CaractereAcceptate.size(); k++)
				cout << Automat[i][j].CaractereAcceptate[k];
			cout << " ";
		}
		cout << endl;
	}
}
int AutomatFinitDeterminist::TestFinalizare(int x)
{
	int i;
	for (i = 0; i < stari_finale.size(); i++)
		if (x == stari_finale[i])
			return 1;
	return 0;
}
int AutomatFinitDeterminist::VerificareLambda(){
string lambda=("$");
for(int i=0; i< stari_finale.size(); i++)
if(word.compare(lambda)==0 && StareInitiala==stari_finale[i])
        return 1;
return 0;
}
int AutomatFinitDeterminist::parcurgere()
{
	queue <pair <char*,int> > coada;
	int i, j;
	if(VerificareLambda()==1)
        return 1;
    if(word.size()==1)
        for (i = 0; i < nr_stari; i++)
            for (j = 0; j < Automat[StareInitiala][i].CaractereAcceptate.size(); j++)
			if (Automat[StareInitiala][i].CaractereAcceptate[j] == word[0])
                for(int p=0; p< stari_finale.size(); p++)
                    if(i==stari_finale[p])
                        return 1;

	for (i = 0; i < nr_stari; i++)
		for (j = 0; j < Automat[StareInitiala][i].CaractereAcceptate.size(); j++)
			if (Automat[StareInitiala][i].CaractereAcceptate[j] == word[0])
			{
				const char* WordAux=word.c_str();

				pair <char*,int> a;
				a.first = new char[100];
				strcpy(a.first, WordAux + 1);
				a.second = i;
				coada.push(a);
			}
	while (!coada.empty())
	{
		pair<char*, int> a;
		a.first = new char[100];
		strcpy(a.first,coada.front().first);
		a.second = coada.front().second;
		coada.pop();
		for (int i = 0; i < nr_stari; i++)
			for (int j = 0; j < Automat[a.second][i].CaractereAcceptate.size(); j++)
				if (Automat[a.second][i].CaractereAcceptate[j] == a.first[0])
					if (strlen(a.first) == 1 && TestFinalizare(i))
						return 1;
					else
					{
						pair< char*, int> b;
						b.first = new char[100];
						strcpy(b.first, a.first + 1);
						b.second = i;
						coada.push(b);
					}
	}
	return 0;
}
void Program(){
    AutomatFinitDeterminist A;
A.citire();
	if (A.parcurgere() == 1)
		{cout << "Cuvantul ";A.GetWord();cout<<" e acceptat de automat!";}
	else
		{cout << "Cuvantul ";A.GetWord();cout<<" nu este acceptat de automat!";}
}
int main()
{	Program();}
